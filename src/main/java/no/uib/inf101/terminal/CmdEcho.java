package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String output = "";
        for (String string : args) {
            output = output + string + " ";
        }
        return output;
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
        return "echo - Gjentar input på ny linje.";
    }

    

}
