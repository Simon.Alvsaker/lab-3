package no.uib.inf101.terminal;

import java.util.Map;

public class CmdMan implements Command{

    private Map<String, Command> commandContext;
    
    @Override
    public void setCommandContext(Map<String, Command> commandContext) {
        this.commandContext = commandContext;
    }

    @Override
    public String run(String[] args) {
        return commandContext.get(args[0]).getManual();
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return "man - Se kommando-manual.";
        
    }
    
}
